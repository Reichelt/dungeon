import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;

public class PanelInformation extends JPanel {
	/**
	 * Create the panel.
	 */
	public PanelInformation() {
		setSize(530, 400);
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 228, 181));
		panel.setBounds(0, 0, 530, 400);
		add(panel);
		panel.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(233, 150, 122));
		panel_1.setBounds(10, 0, 510, 400);
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Hier können die Informationen stehen, wie man das Spiel spielt.");
		lblNewLabel.setFont(new Font("MS UI Gothic", Font.BOLD, 16));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(10, 10, 490, 378);
		panel_1.add(lblNewLabel);
	}
}