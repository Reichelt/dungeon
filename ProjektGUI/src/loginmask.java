import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.awt.Image;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.DebugGraphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.SystemColor;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class loginmask extends JFrame {
	
	private Image imglogo = new ImageIcon(loginmask.class.getResource("/res/dragon.png")).getImage().getScaledInstance(90, 90, Image.SCALE_SMOOTH);
	private JPanel contentPane;
	private JPasswordField txtpassword;
	private JTextField txtUsername;
	private JLabel lblLoginMessage = new JLabel("");
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					loginmask frame = new loginmask();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	/**
	 * Create the frame.
	 */
	public loginmask() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 500);
		contentPane = new JPanel();
		setUndecorated(true);
		setLocationRelativeTo(null);
		contentPane.setLocation(-247, -221);
		contentPane.setBackground(new Color(47, 79, 79));
		contentPane.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel pnlUsername = new JPanel();
		pnlUsername.setBackground(Color.WHITE);
		pnlUsername.setBounds(250, 217, 300, 40);
		contentPane.add(pnlUsername);
		pnlUsername.setLayout(null);

		txtUsername = new JTextField();
		txtUsername.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				if (txtUsername.getText().equals("Username")) {
					txtUsername.setText("");
				} else {
					txtUsername.selectAll();
				}
			}
			@Override
			public void focusLost(FocusEvent e) {
				if (txtUsername.getText().equals("")) {
					txtUsername.setText("Username");
				}
			}
		});
		txtUsername.setBorder(null);
		txtUsername.setHorizontalAlignment(SwingConstants.CENTER);
		txtUsername.setForeground(Color.GRAY);
		txtUsername.setFont(new Font("Arial", Font.PLAIN, 14));
		txtUsername.setText("Username");
		txtUsername.setBounds(40, 5, 220, 30);
		pnlUsername.add(txtUsername);
		txtUsername.setColumns(10);

		JPanel pnlPW = new JPanel();
		pnlPW.setBackground(Color.WHITE);
		pnlPW.setBounds(250, 272, 300, 40);
		contentPane.add(pnlPW);
		pnlPW.setLayout(null);

		txtpassword = new JPasswordField();
		txtpassword.setEchoChar((char)0);
		txtpassword.setText("Password");
		txtpassword.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if (txtpassword.getText().equals("Password")) {
					txtpassword.setEchoChar('●');
					txtpassword.setText("");
				} else {
					txtpassword.selectAll();
				}
			}
			@Override
			public void focusLost(FocusEvent e) {
				if (txtpassword.getText().equals("")) {
					txtpassword.setText("Password");
					txtpassword.setEchoChar((char) 0);
				}
			}
		});
		txtpassword.setBorder(null);
		txtpassword.setHorizontalAlignment(SwingConstants.CENTER);
		txtpassword.setForeground(Color.GRAY);
		txtpassword.setFont(new Font("Arial", Font.PLAIN, 14));
		txtpassword.setToolTipText("");
		txtpassword.setBounds(40, 5, 220, 30);
		pnlPW.add(txtpassword);

		JPanel pnlLogin = new JPanel();
		pnlLogin.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (txtUsername.getText().equals("admin") && txtpassword.getText().equals("admin123")) {
					lblLoginMessage.setText("Login erfolgreich!");
				} else if (txtUsername.getText().equals("") || txtUsername.getText().equals("Username")
						|| txtpassword.getText().equals("") || txtpassword.getText().equals("Password")) {
					lblLoginMessage.setText("Bitte überprüfen Sie ihre Eingaben auf Vollständigkeit und Korrektheit!");
				} else {
					lblLoginMessage.setText("Username und/oder Password ist/sind nicht korrekt!");
				}
			}
			@Override
			public void mouseEntered(MouseEvent arg0) {
				pnlLogin.setBackground(new Color(0, 148, 148));
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				pnlLogin.setBackground(new Color(0, 128, 128));
			}
		});
		pnlLogin.setBackground(new Color(0, 128, 128));
		pnlLogin.setBounds(250, 360, 300, 64);
		contentPane.add(pnlLogin);
		pnlLogin.setLayout(null);

		JLabel lblNewLabel = new JLabel("LOGIN");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 14));
		lblNewLabel.setBounds(10, 11, 280, 42);
		pnlLogin.add(lblNewLabel);

		JLabel lblx = new JLabel("x");
		lblx.setForeground(Color.BLACK);
		lblx.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (JOptionPane.showConfirmDialog(null, "Sind Sie sicher sie wollen diese Anwendung schließen?",
						"Bestätigen", JOptionPane.YES_NO_OPTION) == 0) {
					loginmask.this.dispose();
				}
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				lblx.setForeground(Color.RED);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				lblx.setForeground(Color.BLACK);
			}
		});
		lblx.setFont(new Font("MS UI Gothic", Font.PLAIN, 18));
		lblx.setBackground(Color.BLACK);
		lblx.setForeground(Color.BLACK);
		lblx.setHorizontalAlignment(SwingConstants.CENTER);
		lblx.setBounds(774, 0, 25, 25);
		contentPane.add(lblx);

		JLabel lblIconLogo = new JLabel("");
		lblIconLogo.setHorizontalTextPosition(SwingConstants.CENTER);
		lblIconLogo.setHorizontalAlignment(SwingConstants.CENTER);
		lblIconLogo.setIcon(new ImageIcon(imglogo));
		lblIconLogo.setBounds(250, 70, 300, 120);
		contentPane.add(lblIconLogo);
		lblLoginMessage.setHorizontalAlignment(SwingConstants.CENTER);

		lblLoginMessage.setForeground(new Color(160, 82, 45));
		lblLoginMessage.setFont(new Font("Arial", Font.PLAIN, 12));
		lblLoginMessage.setBounds(200, 323, 400, 26);
		contentPane.add(lblLoginMessage);
		setLocationRelativeTo(null);
	}
}
