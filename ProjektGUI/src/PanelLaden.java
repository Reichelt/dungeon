import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;

public class PanelLaden extends JPanel {

	/**
	 * Create the panel.
	 */
	public PanelLaden() {
		setBounds(0, 0, 530, 400);
		setLayout(null);
		setVisible(true);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 228, 181));
		panel.setBounds(0, 0, 530, 400);
		add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Es wird der letzte Spielstand geladen....");
		lblNewLabel.setFont(new Font("MS UI Gothic", Font.BOLD, 18));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(10, 11, 510, 378);
		panel.add(lblNewLabel);
	}

}