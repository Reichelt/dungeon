import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class PanelEinstellungen extends JPanel {
	/**
	 * Create the panel.
	 */
	public PanelEinstellungen() {
		setSize(530, 400);
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 228, 181));
		panel.setBounds(0, 0, 530, 400);
		add(panel);
		panel.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(233, 150, 122));
		panel_1.setBounds(160, 163, 200, 40);
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblPlatzhalter_2 = new JLabel("Platzhalter");
		lblPlatzhalter_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblPlatzhalter_2.setBounds(10, 11, 180, 18);
		panel_1.add(lblPlatzhalter_2);
		
		JPanel panel_1_1 = new JPanel();
		panel_1_1.setBackground(new Color(233, 150, 122));
		panel_1_1.setBounds(160, 61, 200, 40);
		panel.add(panel_1_1);
		panel_1_1.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Format");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(10, 11, 180, 18);
		panel_1_1.add(lblNewLabel);
		
		JPanel panel_1_2 = new JPanel();
		panel_1_2.setBackground(new Color(233, 150, 122));
		panel_1_2.setBounds(160, 112, 200, 40);
		panel.add(panel_1_2);
		panel_1_2.setLayout(null);
		
		JLabel lblPlatzhalter_3 = new JLabel("Platzhalter");
		lblPlatzhalter_3.setHorizontalAlignment(SwingConstants.CENTER);
		lblPlatzhalter_3.setBounds(10, 11, 180, 18);
		panel_1_2.add(lblPlatzhalter_3);
		
		JPanel panel_1_3 = new JPanel();
		panel_1_3.setBackground(new Color(233, 150, 122));
		panel_1_3.setBounds(160, 214, 200, 40);
		panel.add(panel_1_3);
		panel_1_3.setLayout(null);
		
		JLabel lblPlatzhalter_1 = new JLabel("Platzhalter");
		lblPlatzhalter_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblPlatzhalter_1.setBounds(10, 11, 180, 18);
		panel_1_3.add(lblPlatzhalter_1);
		
		JPanel panel_1_4 = new JPanel();
		panel_1_4.setBackground(new Color(233, 150, 122));
		panel_1_4.setBounds(160, 265, 200, 40);
		panel.add(panel_1_4);
		panel_1_4.setLayout(null);
		
		JLabel lblPlatzhalter = new JLabel("Platzhalter");
		lblPlatzhalter.setHorizontalAlignment(SwingConstants.CENTER);
		lblPlatzhalter.setBounds(10, 11, 180, 18);
		panel_1_4.add(lblPlatzhalter);
		
		JPanel panel_1_5 = new JPanel();
		panel_1_5.setBackground(new Color(233, 150, 122));
		panel_1_5.setBounds(160, 316, 200, 40);
		panel.add(panel_1_5);
		panel_1_5.setLayout(null);
		
		JLabel lblVerlassen = new JLabel("Verlassen");
		lblVerlassen.setHorizontalAlignment(SwingConstants.CENTER);
		lblVerlassen.setBounds(10, 11, 180, 18);
		panel_1_5.add(lblVerlassen);
	}

}