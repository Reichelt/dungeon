import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.border.LineBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;

public class Menu extends JFrame {

	private Image imglogo = new ImageIcon(loginmask.class.getResource("/res/dragon.png")).getImage().getScaledInstance(90, 90, Image.SCALE_SMOOTH);
	private JPanel contentPane;
	
	private PanelLaden panelLaden;
	private PanelNewgame panelNewgame;
	private PanelInformation panelInformation;
	private PanelEinstellungen panelEinstellungen;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu frame = new Menu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Menu() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 500);
		setUndecorated(true);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setForeground(Color.BLACK);
		contentPane.setBackground(new Color(47, 79, 79));
		contentPane.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		panelLaden = new PanelLaden();
		panelNewgame = new PanelNewgame();
		panelInformation = new PanelInformation();
		panelEinstellungen = new PanelEinstellungen();
		
		
		JPanel panelMenu = new JPanel();
		panelMenu.setBorder(new LineBorder(new Color(0, 0, 0)));
		panelMenu.setBackground(new Color(255, 222, 173));
		panelMenu.setBounds(20, 1, 230, 498);
		contentPane.add(panelMenu);
		panelMenu.setLayout(null);
		
		JLabel lblLogo = new JLabel("");
		lblLogo.setHorizontalAlignment(SwingConstants.CENTER);
		lblLogo.setBounds(10, 11, 210, 129);
		lblLogo.setIcon(new ImageIcon(imglogo));
		panelMenu.add(lblLogo);
		
		JPanel panelladen = new JPanel();
		panelladen.addMouseListener(new PanelButtonMouseAdapter(panelladen) {
			@Override
			public void mouseClicked(MouseEvent e) {
				menuClicked(panelLaden);
			}
		});
		panelladen.setBorder(null);
		panelladen.setBackground(new Color(233, 150, 122));
		panelladen.setBounds(1, 206, 228, 40);
		panelMenu.add(panelladen);
		panelladen.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Laden");
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 12));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(10, 11, 208, 18);
		panelladen.add(lblNewLabel);
		
		JPanel panelsettings = new JPanel();
		panelsettings.addMouseListener(new PanelButtonMouseAdapter(panelsettings) {
			@Override
			public void mouseClicked(MouseEvent e) {
				menuClicked(panelEinstellungen);
			}
		});
		panelsettings.setBorder(null);
		panelsettings.setBackground(new Color(233, 150, 122));
		panelsettings.setBounds(1, 360, 228, 40);
		panelMenu.add(panelsettings);
		panelsettings.setLayout(null);
		
		JLabel lblNewLabel_2 = new JLabel("Einstellungen");
		lblNewLabel_2.setFont(new Font("Arial", Font.BOLD, 12));
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setBounds(10, 11, 208, 18);
		panelsettings.add(lblNewLabel_2);
		
		JPanel panelexit = new JPanel();
		panelexit.addMouseListener(new PanelButtonMouseAdapter(panelexit) {
			@Override
			public void mouseClicked(MouseEvent e) {
				loginmask loginmask = new loginmask();
				loginmask.setVisible(true);
				Menu.this.dispose();
			}
		});
		panelexit.setBorder(null);
		panelexit.setBackground(new Color(233, 150, 122));
		panelexit.setBounds(1, 410, 228, 40);
		panelMenu.add(panelexit);
		panelexit.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("Verlassen");
		lblNewLabel_1.setFont(new Font("Arial", Font.BOLD, 12));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setBounds(10, 11, 208, 18);
		panelexit.add(lblNewLabel_1);
		
		JPanel panelinformation = new JPanel();
		panelinformation.setBounds(1, 309, 228, 40);
		panelMenu.add(panelinformation);
		panelinformation.addMouseListener(new PanelButtonMouseAdapter(panelinformation) {
			@Override
			public void mouseClicked(MouseEvent e) {
				menuClicked(panelInformation);
			}
		});
		panelinformation.setBorder(null);
		panelinformation.setBackground(new Color(233, 150, 122));
		panelinformation.setLayout(null);
		
		JLabel lblNewLabel_3 = new JLabel("Informationen");
		lblNewLabel_3.setFont(new Font("Arial", Font.BOLD, 12));
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_3.setBounds(10, 11, 208, 18);
		panelinformation.add(lblNewLabel_3);
		
		JPanel panelneues = new JPanel();
		panelneues.setBounds(1, 257, 228, 40);
		panelMenu.add(panelneues);
		panelneues.addMouseListener(new PanelButtonMouseAdapter(panelneues) {
			@Override
			public void mouseClicked(MouseEvent e) {
				menuClicked(panelNewgame);
			}
		});
		panelneues.setFont(new Font("Arial", Font.BOLD, 12));
		panelneues.setBorder(null);
		panelneues.setBackground(new Color(233, 150, 122));
		panelneues.setLayout(null);
		
		JLabel lblNewLabel_4 = new JLabel("Neues Spiel");
		lblNewLabel_4.setFont(new Font("Arial", Font.BOLD, 12));
		lblNewLabel_4.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_4.setBounds(10, 11, 208, 18);
		panelneues.add(lblNewLabel_4);
		
		JLabel lblX = new JLabel("x");
		lblX.setForeground(Color.BLACK);
		lblX.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if(JOptionPane.showConfirmDialog(null, "Sind Sie sicher sie wollen diese Anwendung schließen?", "Bestätigen", JOptionPane.YES_NO_OPTION) == 0) {
					Menu.this.dispose();
				}
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				lblX.setForeground(Color.RED);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				lblX.setForeground(Color.BLACK);
			}
		});
		lblX.setHorizontalAlignment(SwingConstants.CENTER);
		lblX.setFont(new Font("MS UI Gothic", Font.PLAIN, 18));
		lblX.setBounds(775, 0, 25, 25);
		contentPane.add(lblX);
		
		JPanel panelMainContent = new JPanel();
		panelMainContent.setBounds(260, 50, 530, 400);
		contentPane.add(panelMainContent);
		panelMainContent.setLayout(null);
		
		panelMainContent.add(panelLaden);
		panelMainContent.add(panelNewgame);
		panelMainContent.add(panelInformation);
		panelMainContent.add(panelEinstellungen);
		
		menuClicked(panelLaden);
	}
	
	public void menuClicked(JPanel panel) {
		panelLaden.setVisible(false);
		panelNewgame.setVisible(false);
		panelInformation.setVisible(false);
		panelEinstellungen.setVisible(false);
		
		panel.setVisible(true);
	}
	
	private class PanelButtonMouseAdapter extends MouseAdapter{
		JPanel panel;
		public PanelButtonMouseAdapter(JPanel panel) {
			this.panel = panel;
		}
		@Override
		public void mouseEntered(MouseEvent e) {
			panel.setBackground(new Color(253, 170, 142));
		}
		@Override
		public void mouseExited(MouseEvent e) {
			panel.setBackground(new Color(233, 150, 122));
		}
	}
}
